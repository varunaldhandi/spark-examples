package SparkSQL
import Context.CreateContext

object DFOperations {
  def main(args: Array[String]): Unit = {
    val sqlContext = CreateContext.sqlContext
    val properties = Map("header" -> "true", "inferSchema" -> "true")
    val inspectionsDF = sqlContext.read.options(properties).format("com.databricks.spark.csv").load("/home/cloudera/Downloads/SFFoodProgram_Complete_Data/inspections_plus.csv")
    val violationsDF = sqlContext.read.options(properties).format("com.databricks.spark.csv").load("/home/cloudera/workspace_oxygen/Spark_Practise/Spark_1.6/src/main/resources/SQL/violations_plus.csv")

    inspectionsDF.printSchema()
    violationsDF.printSchema()
    //Selecting specific columns
    val selectDF = violationsDF.select("business_id", "risk_category")
    selectDF.show()

  }
}