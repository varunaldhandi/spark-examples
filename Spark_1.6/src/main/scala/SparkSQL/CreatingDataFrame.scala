package SparkSQL

import org.apache.spark.SparkConf

import org.apache.spark.SparkContext
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.SQLContext

object CreatingDataFrame {
  /**
   * To demonstrate how to create a data frame from an RDD
   * We would need an RDD[Row] and a suitable schema for it
   * Dataframe --> Is a collection of rows with a schema
   */
  def main(args : Array[String]) : Unit = {
    val conf = new SparkConf().setAppName("Creating data frame").setMaster("local[*]")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    
    val numbers = sc.parallelize(Array(1,2,3,4,5,6,7,8,9,10))

    //Creating an RDD[Row]
    val rowRDD = numbers.map(line => Row(line))
    
    //Creating a schema programmatically
    //Schema is nothing but an object of StructType which has a list of StructFields in it
    //You should always end the list with "Nil"
    //csvRDD.schema(schema)
    val schema = StructType(
        StructField("number",IntegerType,true) :: Nil )
        
        //As we have an RDD[Row] and schema ready, we can now create a dataframe using sqlContext.createDataFrame        
    val numbersDF = sqlContext.createDataFrame(rowRDD, schema)
    
    
    numbersDF.show()
  }
}