package SparkSQL

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext
import com.databricks.spark.csv

object WorkingWithCSVFiles {
  def main(args: Array[String]) : Unit = {
    val conf = new SparkConf().setAppName("Working with CSV files").setMaster("local[*]")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    
    val csvFile = "src/main/resources/SQL/violations_plus.csv"
    val options = Map("header" -> "true", "inferSchema" -> "true")
    val csvDF = sqlContext.read.options(options).format("com.databricks.spark.csv").load(csvFile)
    
    csvDF.printSchema()
    
    csvDF.show()
  }
}