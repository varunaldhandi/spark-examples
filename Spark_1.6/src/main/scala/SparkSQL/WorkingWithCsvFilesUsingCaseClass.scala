package SparkSQL

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext
import Context.CreateContext

object WorkingWithCsvFilesUsingCaseClass {
  case class Violations(business_id: Integer, date: Integer, ViolationTypeID: Integer, risk_category: String, description : String)
  def main(args: Array[String]) : Unit = {
    val sc = CreateContext.sc
    val sqlContext = CreateContext.sqlContext
    val csvFile = "src/main/resources/SQL/violations_plus.csv"
//    val csvDF = sqlContext.read.format("com.databricks.spark.csv").load(csvFile)
    val csvRDD = sc.textFile(csvFile).map(_.split(",")).map(v => Violations(v(0).toInt, v(1).toInt, v(2).toInt, v(3), v(4)))
    /**
     * Case classes should be used along with toDF. toDF method is only present in Spark 2.0
     */
  }
}