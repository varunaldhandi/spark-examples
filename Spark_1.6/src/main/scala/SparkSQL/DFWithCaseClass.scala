package SparkSQL
import Context.CreateContext

case class Numbers(number: Int)
object DFWithCaseClass {
  def main(args: Array[String]) : Unit = {
    val sqlContext = CreateContext.sqlContext
    val numbers = Seq(Numbers(1), Numbers(2), Numbers(3), Numbers(4), Numbers(5))
    
    val numDF = sqlContext.createDataFrame(numbers)
    numDF.printSchema()
  }
}