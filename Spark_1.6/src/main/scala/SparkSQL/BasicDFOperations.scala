package SparkSQL
import Context.CreateContext
object BasicDFOperations {
  def main(args: Array[String]) : Unit = {
    val sqlContext = CreateContext.sqlContext
    val csvFile = "src/main/resources/SQL/violations_plus.csv"
    val properties = Map("head" -> "true", "inferschema" -> "true")
    val csvDF = sqlContext.read.options(properties).format("com.databricks.spark.csv").load(csvFile)
    
    csvDF.show()
    
    //Count of data frame
    val cnt = csvDF.count
    println("Count :" + cnt)
    
    //count of business_ids
    val groupedDF = csvDF.groupBy("business_id").count
    println("Showing first 20 rows of groupedDF")
    groupedDF.show()
  }
}