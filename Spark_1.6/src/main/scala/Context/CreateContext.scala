package Context

import org.apache.spark.sql.SQLContext
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

object CreateContext {
  val conf = new SparkConf().setAppName("Working with csv files using case class").setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
   def main(args: Array[String]) : Unit = {
    
    def getSparkContext() : SparkContext = sc
    def getSQLContext() : SQLContext = sqlContext
  }
  
}