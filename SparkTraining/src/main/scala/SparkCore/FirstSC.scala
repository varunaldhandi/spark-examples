package SparkCore

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

object FirstSC {
  def main(args: Array[String]) : Unit = {
    //Spark 1.x 
    
    val conf = new SparkConf().setAppName("First Spark Context").setMaster("local[*]")
    val sc = new SparkContext(conf)
    
//    val nums = sc.parallelize(Array(1,2,3,4,5), 2)
//    nums.foreach(println)
    val wordCntFile = sc.textFile("/home/cloudera/Downloads/FIFA 2018 Statistics.csv") //Reading a file will create an RDD
    val wordCnt = wordCntFile.flatMap(line => line.split(",")).map(word => (word,1)).reduceByKey(_+_)
    
    wordCnt.take(20).foreach(println)
    
    
  }
}