package SparkCore

import org.apache.spark.sql.SparkSession

object SparkSessionSample {
  def main(args: Array[String]) : Unit = {
    //Spark 2.x style
    val spark = SparkSession.builder().appName("Spark Session Sample").master("local[*]").getOrCreate()
    val nums= spark.sparkContext.parallelize(Array(1,2,3,4,5,6,7,8,9), 2)
    
    nums.foreach(println)
  }
}