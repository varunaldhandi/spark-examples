package SparkStreaming

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

object NetworkWordCountStructuredStreaming {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .master("local[2]")
      .appName("Network word count with structured streaming")
      .getOrCreate()

    val lines = spark.readStream
      .format("socket")
      .option("host", "localhost")
      .option("port", 9999).load()
    // // Importing spark.implicits._ to avoid getting "Unable to find encoder for type stored in dataset" error while doing "as[String]"
    
    import spark.implicits._
    val words = lines.as[String].flatMap(_.split(" "))

    //Generating running word count
    val wordCounts = words.groupBy("value").count()

    //Start running queries that prints the running counts to the console

    val query = wordCounts.writeStream
      .outputMode("complete")
      .format("console")
      .start()
   query.awaitTermination()
  }
}