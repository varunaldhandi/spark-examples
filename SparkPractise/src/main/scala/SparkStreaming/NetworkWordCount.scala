package SparkStreaming

import org.apache.spark.SparkConf
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.Seconds

object NetworkWordCount {
  def main(args: Array[String]) : Unit = {
    val conf = new SparkConf().setMaster("local[2]")
                              .setAppName("Network Word Count")
    val streamingContext = new StreamingContext(conf, Seconds(1))
    
    //Creating a DStream of lines
    val lines = streamingContext.socketTextStream("localhost", 9999)
    val wordCount = lines.flatMap(_.split(" ")).map(word => (word,1)).reduceByKey(_+_)
    
    wordCount.print()
    streamingContext.start()
    streamingContext.awaitTermination()
  }
}