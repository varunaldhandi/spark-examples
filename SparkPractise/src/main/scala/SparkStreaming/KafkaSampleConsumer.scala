package SparkStreaming

import java.util.Properties
import java.util.Collections
import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecord, KafkaConsumer}

import com.typesafe.config.ConfigFactory

import scala.collection.JavaConversions._

object KafkaSampleConsumer {
  def main(args: Array[String]) : Unit = {
    val config = ConfigFactory.load
    val envConfig = config.getConfig("dev")
    
    val props = new Properties()
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,envConfig.getString("bootstrap.server"))
    props.put(ConsumerConfig.GROUP_ID_CONFIG,"1")
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringDeserializer")
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
    
   val consumer = new KafkaConsumer[String, String](props)
    consumer.subscribe(Collections.singletonList("Kafka-Testing"))
    while(true){
      val records = consumer.poll(500)
      for (record <- records.iterator()) {
        println("Received message: (" + record.key() + ", " + record.value() + ") at offset " + record.offset())
      }
    /*val consumer = new KafkaConsumer[String,String](props)
    consumer.subscribe(topics)*/
   } 
  }
}