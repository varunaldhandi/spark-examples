package SparkStreaming
import com.typesafe.config.ConfigFactory
import java.util.Properties
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}

import scala.sys.process._


object KafkaSample {
  def main(args: Array[String]) : Unit = {
    val conf = ConfigFactory.load
    val envConfig = conf.getConfig("dev")
    
    val props = new Properties()
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, envConfig.getString("bootstrap.server"))
    props.put(ProducerConfig.CLIENT_ID_CONFIG,"Sample Kafka Producer")
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringSerializer")
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
    
    val producer = new KafkaProducer[String,String](props)
    
    def processLine(line: String, producer: KafkaProducer[String, String]): Unit = {
      val data = new ProducerRecord[String, String]("Kafka-Testing", "Key", line)
      producer.send(data)
    }

    val file = "/opt/gen_logs/logs/access.log"

    val tail = Seq("tail", "-f", file)

    tail.lineStream.foreach(processLine(_, producer))
  
    
  }
}