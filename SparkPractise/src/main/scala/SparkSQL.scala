import org.apache.spark.sql.SparkSession


object SparkSQL {
  def main(args: Array[String]) : Unit = {
    val spark = SparkSession.builder()
              .appName("Spark SQL Practise")
              .master("local")
              .getOrCreate()
     val rdd = spark.sparkContext.parallelize(Array(1,2,3,4,5))
     rdd.collect().foreach(println)
  }
}