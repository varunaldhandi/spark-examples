package SparkSQL

import org.apache.spark.sql.SparkSession

object FileFormats {
  def main(args: Array[String]) : Unit = {
    val spark = SparkSession.builder().appName("File Formats").master("local").getOrCreate()
    val parquetFile = "src/main/resources/SQL/meetup_parquet.parquet"
    
    val parquetDF = spark.read.parquet(parquetFile)
    
    parquetDF.printSchema()
    parquetDF.show()
  }
}