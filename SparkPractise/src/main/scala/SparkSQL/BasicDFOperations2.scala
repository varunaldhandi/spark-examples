package SparkSQL

import org.apache.spark.sql.SparkSession

object BasicDFOperations2 {
  def main(args: Array[String]) : Unit = {
    val spark = SparkSession.builder().appName("Basic DF operations").master("local").getOrCreate()
    val prqDF = spark.read.parquet("src/main/resources/SQL/userdata1.parquet")
    prqDF.head(10).foreach(println) //It prints each element of the array
    prqDF.show(10) //It prints the elements of dataframe in a tabular way
    
    //Selecting columns
    prqDF.select("id", "first_name","last_name","email","gender").where("gender == 'Male'").show(10)
  }
}