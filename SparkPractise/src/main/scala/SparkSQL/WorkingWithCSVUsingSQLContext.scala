package SparkSQL

import org.apache.spark.SparkConf

import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext

object WorkingWithCSVUsingSQLContext {
  def main(args: Array[String]) : Unit = {
    val conf = new SparkConf()
              .setAppName("Working with CSV files using spark 1.x")
              .setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val csvFile = "src/main/resources/SQL/Testers.csv"
    
    //val csvDF = sqlContext.read.option("header","true").option("inferSchema","true").csv(csvFile)
    val csvDF = sqlContext.read.options(Map("header" -> "true", "inferSchema" -> "true")).csv(csvFile)
    
    csvDF.printSchema()
    csvDF.show()
    println("count : "+ csvDF.count)
  }
}