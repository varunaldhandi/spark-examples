package SparkSQL

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StructField

object CreatingDataFrame {
  /**
   * Creating a data frame --> It requires a collection of Rows as well as a schema
   * collection of rows can be created using Row[RDD]
   * Schema can be created using StructType
   * */
  def main(args: Array[String]) : Unit = {
    val spark = SparkSession.builder()
    .appName("Creating a dataframe")
    .master("local")
    .getOrCreate()
    
    val rdd = spark.sparkContext.parallelize(Array(1,2,3,4,5,6,7,8,9,10))
   
    
    //Creating an RDD of Row
    
    val rowRDD = rdd.map(line => Row(line))
    
    //Creating Schema
    val schema = StructType(
                  StructField("Numbers",IntegerType,true) :: Nil)
    //Now that we have a collection of rows and schema, we shall create a data frame using them 
                  
    //val df = spark.createDataFrame(rowRDD, schema)
                  val df = spark.createDataFrame(rowRDD, schema)
    
    //printing schema
    println("Schema of the data frame")
    df.printSchema()
    
    println("Printing the contents of the data frame")
    df.show()
    
    
  }
}