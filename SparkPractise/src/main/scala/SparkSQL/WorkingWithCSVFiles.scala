package SparkSQL

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.LongType
import org.apache.spark.sql.types.IntegerType

object WorkingWithCSVFiles {
  def main(args: Array[String]) : Unit = {
    val spark = SparkSession.builder()
      .appName("Working with CSV files")
      .master("local")
      .getOrCreate()
      
      //Specifying the options using a map
      val properties = Map("header" -> "true", "inferSchema" -> "true")
     val csvFile = "src/main/resources/SQL/Testers.csv"
     val csvDF = spark.read.options(properties).csv(csvFile)
     println("Before applying schema explicitly:")
     csvDF.printSchema()
     csvDF.show()
     
     val schema = StructType(
       StructField("DBN",StringType,true) ::
       StructField("School",StringType,true) ::
       StructField("Year of SHST",LongType,true) ::
       StructField("Grade",LongType,true) ::
       StructField("Enrollment on 10/31",LongType,true) ::
       StructField("Registered",IntegerType,true) ::
       StructField("took SHSAT",LongType,true) :: Nil    
     )
      
     val csvFileWithSchema = spark.read.option("header","true").schema(schema).csv(csvFile)
     println("After applying schema explicitly :")
     csvFileWithSchema.printSchema()
     
     
  }
}