package SparkSQL

import org.apache.spark.sql.SparkSession

object TempTables {
  def main(args : Array[String]) : Unit = {
    val spark = SparkSession.builder()
      .appName("Temp Table over dataframes")
      .master("local")
      .enableHiveSupport()
      .getOrCreate()
     val prqDF = spark.read.parquet("src/main/resources/SQL/userdata1.parquet")
     
     //Spark 1.x --> registerTempTable()
     prqDF.registerTempTable("prq_temp_table")
     println("==========In Spark 1.x ===========")
     spark.sql("select * from prq_temp_table limit 10").show(10)
     
     println("==========In Spark 2.x ===========")
     prqDF.createOrReplaceTempView("prqTemp2")
     spark.sql("select * from prqTemp2 limit 10").show()
  }
}