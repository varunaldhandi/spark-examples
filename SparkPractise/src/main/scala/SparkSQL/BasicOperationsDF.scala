package SparkSQL

import org.apache.spark.sql.SparkSession

object BasicOperationsDF {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("Basic DF operations").master("local").getOrCreate()
    val parquetDF = spark.read.parquet("src/main/resources/SQL/userdata1.parquet")

    //Printing schema
    parquetDF.printSchema()

    parquetDF.show()

    //printing schema using "schema" function
    val parSchema = parquetDF.schema
    println("StructType schema : " + parSchema)

    //Printing columns --> columns function returns an array of columns
    val cols = parquetDF.columns
    println("Columns of the dataframe : " + cols.mkString(","))

    //dtypes -> prints the column names and their data types --> It returns an array

    val colAndTypes = parquetDF.dtypes
    println("Column names and types")
    colAndTypes.foreach(println)
    
    //To create an array by taking the first n rows of another DF, we should use head(n) function
    val firstFive = parquetDF.head(5)
    firstFive.foreach(println)
  }
}