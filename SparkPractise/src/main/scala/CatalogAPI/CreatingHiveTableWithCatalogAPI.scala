package CatalogAPI

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StringType

object CreatingHiveTableWithCatalogAPI {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("Creating hive table with catalog API")
      .master("local")
      .getOrCreate()
    val stocks = spark.read
      .options(Map("header" -> "true", "inferSchema" -> "true"))
      .csv("/home/cloudera/Downloads/nse-stocks-data.csv")
    val schema = StructType(
      StructField("movie", IntegerType, false)
        :: StructField("title", StringType, false)
        :: StructField("genre", StringType, false)
        :: Nil)
    val catalog = spark.catalog
    catalog.createTable("spark_course.movies", "parquet", schema, Map("comments" -> "Table created with catalog API"))
  }
}