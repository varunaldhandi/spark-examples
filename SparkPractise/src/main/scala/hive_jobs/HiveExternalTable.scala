package hive_jobs

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.SaveMode

object HiveExternalTable {
  def main(args: Array[String]) : Unit = {
    val spark = SparkSession.builder()
      .appName("Temp Table over dataframes")
      .master("local")
      .enableHiveSupport()
      .getOrCreate()
     val prqDF = spark.read.parquet("src/main/resources/SQL/userdata1.parquet")
     
     //Creating external hive table --> using option("path","/path/to/table")
     prqDF.write.option("path","hdfs://192.168.0.103:8020/hive_external/external_parquet")
           .mode(SaveMode.Overwrite)
           .saveAsTable("practise.external_parquet")
     spark.sql("select * from practise.external_parquet limit 10").show()
  }
}