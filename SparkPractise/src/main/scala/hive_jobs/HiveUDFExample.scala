package hive_jobs

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.udf

object HiveUDFExample {
  def main(args: Array[String]) : Unit = {
    val spark = SparkSession.builder()
      .appName("Temp Table over dataframes")
      .master("local")
      .enableHiveSupport()
      .getOrCreate()
     val prqDF = spark.read.parquet("src/main/resources/SQL/userdata1.parquet")
     
     //2. Converting the function to UDF
     val toLowerUDF = udf[String, String](toLower)
     
     //3. Using the UDF in spark SQL
     //spark.sql("select toLowerUDF(country) from part_parq").show()
     prqDF.select(toLowerUDF(prqDF("country"))).show()
     
  }
  //1. Creating a function
  def toLower(s: String) : String = s.toLowerCase()
}