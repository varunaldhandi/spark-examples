package hive_jobs

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.SaveMode

object HiveExternalPartitionedTable {
  def main(args: Array[String]) : Unit = {
    val spark = SparkSession.builder()
      .appName("Temp Table over dataframes")
      .master("local")
      .enableHiveSupport()
      .getOrCreate()
     val prqDF = spark.read.parquet("src/main/resources/SQL/userdata1.parquet")
     prqDF.write.partitionBy("country")
                 .mode(SaveMode.Overwrite)
                 .saveAsTable("practise.part_parq")
     spark.sql("select country,count(*) as cnt from practise.part_parq group by 1").show()
  }
}