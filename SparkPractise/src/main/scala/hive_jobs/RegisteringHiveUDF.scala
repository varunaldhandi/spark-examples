package hive_jobs

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.udf

object RegisteringHiveUDF {
  def main(args: Array[String]) : Unit = {
    val spark = SparkSession.builder()
      .appName("Registering Hive UDF")
      .master("local")
      .enableHiveSupport()
      .getOrCreate()
     val stocks = spark.read.options(Map("header" -> "true", "inferSchema" -> "true")).csv("/home/cloudera/Downloads/nse-stocks-data.csv")
     stocks.createOrReplaceTempView("stocks_tb")
     
     //2. Convert the function into UDF
     val avgUDF = udf[Double, Double, Double](averageFunc)
     
     //3. Register the UDF
     spark.sqlContext.udf.register("averageUDF",avgUDF)
     
     //4. Using UDF in the SQL query
     spark.sql("select SYMBOL, HIGH, LOW, averageUDF(HIGH, LOW) from stocks_tb").show()
     
     
  }
  //1, Creating the function
  def averageFunc(num1: Double,num2: Double) : Double = (num1 + num2)/2.0
}